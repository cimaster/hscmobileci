#!/bin/bash
codepath=$1
jobpath=$2
tarname=$3
pullhash=$4
branch=$5
cd $codepath

if [[ -z $branch ]]
then
  echo Not changing branch
else
  git checkout $branch
fi

#Update the local Repo
if [[ -z $pullhash ]]
then
  echo No hash value mentioned
  git pull
  git submodule init
  git submodule update --init --recursive
else
  echo Hash value is $pullhash
  git pull
  git submodule init
  git submodule update --init --recursive
  git reset --hard $pullhash
fi

#Zip the updated repo
tar cf ../$tarname .
mv ../$tarname $jobpath
cd $jobpath
tar xvf $tarname
rm $tarname