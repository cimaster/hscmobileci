#!/bin/bash
project=$1
branch=$2
if [[ -z $branch ]]
then
  cd $project
  echo "No branch value mentioned, so running on following branch - "
  git branch
else
  cd $project
  echo Branch name is $branch
  git checkout $branch
fi
old=`git rev-parse HEAD`
echo pullhash=$old>$WORKSPACE/current_hash
git pull --prune
update=`git rev-parse HEAD`
echo pullhash=$update>$WORKSPACE/new_hash
if [[ $old -ne $update ]]
then
	echo No changes found>$WORKSPACE/test1
else
	echo Changes found>$WORKSPACE/test2
fi