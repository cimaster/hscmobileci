#!/bin/bash
#Usage
#./updateconfig.sh ${BUILD_NUMBER} configfilepath

jenkinsbuild=$1
filename=$2
echo Jenkins Build : ${jenkinsbuild}
# Take extension available in a filename
ext=${filename##*\.}
case "$ext" in
        plist) echo "$filename : iOS config file"
        #Get the Application Release version from Info.plist - iOS
        xml sel -t -v "//key[contains(text(),\"CFBundleVersion\")]/following-sibling::string[1]/text()" $filename>currentrelease
        currentversion=$(head -n 1 currentrelease)
        echo Current Version : $currentversion
        #Concatenate the bumped build number to versionc
        updatedversion=${currentversion}.${jenkinsbuild}
        echo Updated Build : $updatedversion
        #Append the bumped build number to version
        xml ed --inplace -u "//key[contains(text(),\"CFBundleVersion\")]/following-sibling::string[1]/text()" -v $updatedversion $filename
        xml ed --inplace -u "//key[contains(text(),\"CFBundleShortVersionString\")]/following-sibling::string[1]/text()" -v $updatedversion $filename
        ;;
        xml) echo "$filename : Android config file"
        #Get the Application Release version from AndroidManifest.xml - Android
        xml sel -t -v "/manifest/@android:versionName" $filename>currentrelease
        currentversion=$(head -n 1 currentrelease)
        echo Current Build : $currentversion
        #Concatenate the bumped build number to versionc
        updatedversion=${currentversion}-build${jenkinsbuild}
        echo Updated Build : $updatedversion
        #Append the bumped build number to version
        xml ed --inplace -u "/manifest/@android:versionName" -v $updatedversion $filename
	;;
esac
